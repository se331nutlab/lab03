import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../entity/student';
@Injectable({
  providedIn: 'root'
})
export class MyStudentImplService extends StudentService {
  constructor(private http: HttpClient) {
    super();
  }
  getStudents(): Observable<Student[]> {
    return this.http.get<Student[]>('https://s3-ap-southeast-1.amazonaws.com/se331/people.json');
  }
}
