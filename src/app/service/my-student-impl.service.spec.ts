import { TestBed, inject } from '@angular/core/testing';

import { MyStudentImplService } from './my-student-impl.service';

describe('MyStudentImplService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyStudentImplService]
    });
  });

  it('should be created', inject([MyStudentImplService], (service: MyStudentImplService) => {
    expect(service).toBeTruthy();
  }));
});
